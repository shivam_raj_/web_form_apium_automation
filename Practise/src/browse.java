//import java.net.MalformedURLException;
import java.net.MalformedURLException;

import org.openqa.selenium.Keys;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

//import io.appium.java_client.android.AndroidDriver;
//import io.appium.java_client.android.AndroidElement;
public class browse extends baseChrome {

	public static void main(String[] args) throws MalformedURLException, InterruptedException {
		// TODO Auto-generated method stub
		
		AndroidDriver<AndroidElement> driver=Capabilities();
		System.out.println("Test Case 1 : Invoke web_app URL " + "\n" );
		driver.get("http://testweb.omnicuris.com/user/login ");
		Thread.sleep(5000);
		System.out.println("Web_app page loaded successfully" );
		System.out.println("Result: Test case 1 Pass successfully" + "\n" );
				
		
		System.out.println("Test Case 2 : Login using (Invalid Email and Valid Password)" );
		
		AndroidElement input1= driver.findElementByXPath("//*[@id=\'loginTab\']/div/div/form/div[1]/div[1]/div/div[1]/input");
		input1.sendKeys("9820023789");
		driver.hideKeyboard();
		Thread.sleep(2000);
		driver.findElementByXPath("//*[@id=\'loginTab\']/div/div/form/div[1]/div[2]/div/div[1]/input").sendKeys("Rajshivam139@");
		driver.hideKeyboard();
		Thread.sleep(2000);
		driver.findElementByXPath("//*[@id=\'loginTab\']/div/div/form/div[1]/div[4]/button/div").click();
		Thread.sleep(3000);
		System.out.println("Dialogue shown on screen (No user account found) ");
		System.out.println("Result: Test case 2 Pass successfully" + "\n"  );
		
		Thread.sleep(5000);
		System.out.println("Now refreshing the screen (reloading the page)");
		driver.get("http://testweb.omnicuris.com/user/login ");
		Thread.sleep(5000);
		System.out.println("Page reloaded successfully and it is ready for another test" );
  
		
		
		System.out.println("Test Case 3 : Login using (Valid Email and Invalid Password)" );
		driver.findElementByXPath("//*[@id=\'loginTab\']/div/div/form/div[1]/div[1]/div/div[1]/input").sendKeys("7014856665");
		driver.hideKeyboard();
		Thread.sleep(2000);
		driver.findElementByXPath("//*[@id=\'loginTab\']/div/div/form/div[1]/div[2]/div/div[1]/input").sendKeys("rajshivam123!@#");
		driver.hideKeyboard();
		Thread.sleep(2000);
		driver.findElementByXPath("//*[@id=\'loginTab\']/div/div/form/div[1]/div[4]/button/div").click();
		Thread.sleep(3000);
		System.out.println("Dialogue shown on screen (Invalid Credentials) ");
		System.out.println("Result: Test case 3 Pass successfully" + "\n" );
		Thread.sleep(5000);
		System.out.println("Now refreshing the screen (reloading the page)");
		driver.get("http://testweb.omnicuris.com/user/login ");
		Thread.sleep(5000);
		System.out.println("Page reloaded successfully and it is ready for another test" );
		
		System.out.println("Test Case 4 : Login using (InValid Email and Invalid Password)" );
		driver.findElementByXPath("//*[@id=\'loginTab\']/div/div/form/div[1]/div[1]/div/div[1]/input").sendKeys("9820023789");
		driver.hideKeyboard();
		Thread.sleep(2000);
		driver.findElementByXPath("//*[@id=\'loginTab\']/div/div/form/div[1]/div[2]/div/div[1]/input").sendKeys("rajshivam123!@#");
		driver.hideKeyboard();
		Thread.sleep(2000);
		driver.findElementByXPath("//*[@id=\'loginTab\']/div/div/form/div[1]/div[4]/button/div").click();
		Thread.sleep(3000);
		System.out.println("Dialogue shown on screen (No user account found) ");
		System.out.println("Result: Test case 4 Pass successfully" + "\n"  );
		Thread.sleep(5000);
		System.out.println("Now refreshing the screen (reloading the page)");
		driver.get("http://testweb.omnicuris.com/user/login ");
		Thread.sleep(5000);
		System.out.println("Page reloaded successfully and it is ready for another test" );
		
		System.out.println("Test Case 5 : Login using (Empty Email and Empty Password)" );
		driver.findElementByXPath("//*[@id=\'loginTab\']/div/div/form/div[1]/div[1]/div/div[1]/input").sendKeys("");
		driver.hideKeyboard();
		Thread.sleep(2000);
		driver.findElementByXPath("//*[@id=\'loginTab\']/div/div/form/div[1]/div[2]/div/div[1]/input").sendKeys("");
		driver.hideKeyboard();
		Thread.sleep(2000);
		driver.findElementByXPath("//*[@id=\'loginTab\']/div/div/form/div[1]/div[4]/button/div").click();
		Thread.sleep(3000);
		System.out.println(" Highlight the email and password field ");
		System.out.println("Result: Test case 5 Pass successfully" + "\n"  );
		Thread.sleep(5000);
		System.out.println("Now refreshing the screen (reloading the page)");
		driver.get("http://testweb.omnicuris.com/user/login ");
		Thread.sleep(5000);
		System.out.println("Page reloaded successfully and it is ready for another test" );		
		
		
		System.out.println("Test Case 6 : Login using (Valid Email and Valid Password" );
		driver.findElementByXPath("//*[@id=\'loginTab\']/div/div/form/div[1]/div[1]/div/div[1]/input").sendKeys("7014856665");
		driver.hideKeyboard();
		Thread.sleep(2000);
		driver.findElementByXPath("//*[@id=\'loginTab\']/div/div/form/div[1]/div[2]/div/div[1]/input").sendKeys("Rajshivam139@");
		driver.hideKeyboard();
		Thread.sleep(2000);
		driver.findElementByXPath("//*[@id=\'loginTab\']/div/div/form/div[1]/div[4]/button/div").click();
		Thread.sleep(3000);
		System.out.println("Dashboard screen loaded successfully" );
		System.out.println("Result: Test case 6 Pass successfully" + "\n" );
		Thread.sleep(1000);
		

		System.out.println("Now, user will log out itself from the dashbaord and navigate to login page and then refresh the screen for next execution" );
		driver.findElementByXPath("//*[@id='app\']/div[1]/div[1]/div/div/div/div[1]/div/div[1]/div").click();
		Thread.sleep(1000);
		driver.findElementByXPath("//*[@id=\'app\']/div[1]/div[2]/ul/li[8]/a/a").click();
		Thread.sleep(3000);

		driver.findElementByXPath("//*[@id=\'app\']/div[1]/div[1]/div/div/div/div[1]/div/div/div/div[2]").click();
		Thread.sleep(1000);
		System.out.println("Page reloaded successfully and it is ready for another test" );
		driver.findElementByXPath("//*[@id=\'app\']/div[1]/div[2]/div/div/a/div/a").click();
		Thread.sleep(3000);

		
		Thread.sleep(5000);
		System.out.println("Now refreshing the screen (reloading the page)");
		driver.get("http://testweb.omnicuris.com/user/login ");
		Thread.sleep(5000);
		System.out.println("Page reloaded successfully and it is ready for another test" );
		
		System.out.println("Test Case 7 : Click on Register button to open registration form " );
		
		driver.findElementByXPath("//*[@id=\'loginapp\']/div[7]/div/div/div/div[1]/div/div/div/div[3]/a").click();
//		driver.hideKeyboard();
		System.out.println("Test Case 7 Pass successfully, now user can fill the registration form " + "\n" );
		
		System.out.println("Test Case 8 : Fill the registration form for the user who has already registered" );
		
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[1]/div[2]/div/div[1]/input").sendKeys("Hemant");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[1]/div[3]/div/div[1]/input").sendKeys("Raj");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[2]/div[1]/div/div[1]/input").sendKeys("shivamraj139@gmail.com");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[2]/div[2]/div/div[1]/input").sendKeys("7073633956");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[2]/div[3]/div/div[1]/input").sendKeys("Shivamraj@");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[2]/div[4]/div/div[1]/input").sendKeys("123444444");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id='inspire']/div/form/div[2]/div[5]").click();

		driver.findElementByXPath("//*[@id=\'loginapp\']/div[6]/div/div/div[1]/a/div/div").click();

		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[4]/div/div/div[1]/div").click();
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[5]/button/div").click();
		driver.hideKeyboard();
		System.out.println("Test Case 9 : Check that dialogue should will visible if user is alreday registered" );
		System.out.println("Test cases 8,9 Pass successfully as Dialogue box displayed which shows that phone number is already registered and it restrict the dupliacte entry" + "\n");
		Thread.sleep(3000);
		
		
		
		
		System.out.println("Now refreshing the screen (reloading the page)");
		driver.get("http://testweb.omnicuris.com/user/login ");
		Thread.sleep(5000);
		System.out.println("Page reloaded successfully and it is ready for another test" + "\n" );
		
		System.out.println("Test Case 10 :Fill the registration form using dupliacte email" );
		
		driver.findElementByXPath("//*[@id=\'loginapp\']/div[7]/div/div/div/div[1]/div/div/div/div[3]/a").click();
//		driver.hideKeyboard();
		
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[1]/div[2]/div/div[1]/input").sendKeys("Hemant");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[1]/div[3]/div/div[1]/input").sendKeys("Raj");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[2]/div[1]/div/div[1]/input").sendKeys("shivamraj139@gmail.com");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[2]/div[2]/div/div[1]/input").sendKeys("8078686849");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[2]/div[3]/div/div[1]/input").sendKeys("Shivamraj@");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[2]/div[4]/div/div[1]/input").sendKeys("123444444");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id='inspire']/div/form/div[2]/div[5]").click();

		driver.findElementByXPath("//*[@id=\'loginapp\']/div[6]/div/div/div[1]/a/div/div").click();

		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[4]/div/div/div[1]/div").click();
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[5]/button/div").click();
		driver.hideKeyboard();
		System.out.println("\"Test cases 10 Pass successfully as Dialogue box displayed which shows that email id is already registered and it restrict the dupliacte entry" + "\n");
		Thread.sleep(3000);	
		
		
		System.out.println("Now refreshing the screen (reloading the page)");
		driver.get("http://testweb.omnicuris.com/user/login ");
		Thread.sleep(5000);
		System.out.println("Page reloaded successfully and it is ready for another test" + "\n" );
		
		System.out.println("Test Case 11 :Fill the registration for new user" );
		
		driver.findElementByXPath("//*[@id=\'loginapp\']/div[7]/div/div/div/div[1]/div/div/div/div[3]/a").click();
//		driver.hideKeyboard();
		
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[1]/div[2]/div/div[1]/input").sendKeys("Hemant");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[1]/div[3]/div/div[1]/input").sendKeys("Raj");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[2]/div[1]/div/div[1]/input").sendKeys("abcd@gmail.com");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[2]/div[2]/div/div[1]/input").sendKeys("9931945102");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[2]/div[3]/div/div[1]/input").sendKeys("Shivamraj@");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[2]/div[4]/div/div[1]/input").sendKeys("123643844");
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id='inspire']/div/form/div[2]/div[5]").click();

		driver.findElementByXPath("//*[@id=\'loginapp\']/div[6]/div/div/div[1]/a/div/div").click();

		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[4]/div/div/div[1]/div").click();
		driver.hideKeyboard();
		driver.findElementByXPath("//*[@id=\'inspire\']/div/form/div[5]/button/div").click();
		driver.hideKeyboard();
		System.out.println("Enter the valid OTP and then click on verify button" + "\n");
		Thread.sleep(3000);
		System.out.println("Test Case 11 pass successfully, Cingrats user created successfully");
		
		
		
		
		
		
		
		
	}

}
